//$$---- Form CPP ----
//---------------------------------------------------------------------------
#include "main.h"
//---------------------------------------------------------------------------
#pragma hdrstop
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner) {
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender) {
	OpenPictureDialog1->Execute();
	char *name = OpenPictureDialog1->FileName.c_str();
	BYTE* picBuf =	OpenNewPicture(name);
	picBuf = RGBRegionGrowing(picBuf, 57);
	ShowPicture(picBuf);
	WritePicture(picBuf, "pic1.bmp");
}
//---------------------------------------------------------------------------
