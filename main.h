#include <vcl.h>
#include <math.h>
#include <windows.h>
//---------------------------------------------------------------------------
#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
USEFORM("Unit1.cpp", Form1);
USEFORM("Unit2.cpp", Form2);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
const float PI = 3.1415;
//---------------------------------------------------------------------------
BYTE* OpenNewPicture(const char* name);
BYTE* RGBRegionGrowing(
						BYTE* picBuf,
						const float barrier
				);
BYTE* RGBtoGray (BYTE* picBuf);
void 	StaticSectorGrowing(
					BYTE* pic,
					const float barrier,
					const long start,
					const long& w,
					const long& h,
					long* map,
					long* cSec,
					float* MxB,
					float* MxG,
					float* MxR,
					long indexMap
			);
void 	DrawHistogram (
					BYTE* picBuf,
					BYTE amountHist,
					BYTE minColor,
					BYTE maxColor
			);
void 	WritePicture (
					const BYTE* picBuf,
					const char* name
			);
void 	ShowPicture   (const BYTE* picBuf);
long 	otsuThreshold (BYTE* picBuf);
