//$$---- EXE CPP ----
#include "main.h"
#pragma hdrstop
USEFORM("Unit1.cpp", Form1);
USEFORM("Unit2.cpp", Form2);
//---------------------------------------------------------------------------
TForm1 *Form1;
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	try {
		Application->Initialize();
		Application->CreateForm(__classid(TForm1), &Form1);
		Application->CreateForm(__classid(TForm2), &Form2);
		Application->Run();
	}
	catch (Exception &exception) {
		Application->ShowException(&exception);
	}
	catch (...)	{
		try {
			throw Exception("");
		}
		catch (Exception &exception) {
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//----------------------------[MAIN FUNCTIONS]-------------------------------
BYTE* OpenNewPicture(const char* name) {
	if (name == 0x0) {
		Form1->OpenPictureDialog1->FileName = "";
		if (!(Form1->OpenPictureDialog1->Execute())) {
			return 0;
		}
		if (Form1->OpenPictureDialog1->FileName == "") {
			return 0;
		}
		name = Form1->OpenPictureDialog1->FileName.c_str();
	}

	 HANDLE picFile; // picture file

	picFile = CreateFile(
							name,                        // open file
							GENERIC_READ,                // open for reading
							FILE_SHARE_READ,             // share for reading
							NULL,                        // no security
							OPEN_EXISTING,               // existing file only
							FILE_ATTRIBUTE_NORMAL,       // normal file
							NULL                         // no attr. template
					 	);


	if ( picFile == INVALID_HANDLE_VALUE ) {
		ShowMessage( "Can't open file" );
		CloseHandle( picFile ) ;
		return 0;
	}

	 DWORD picFileSize = GetFileSize( picFile, NULL );

	if ( picFileSize == INVALID_FILE_SIZE ) {
		ShowMessage( "Can't get file size" );
		CloseHandle( picFile ) ;
		return 0;
	}

	BYTE* picFileBuffer;
	picFileBuffer = new BYTE[ picFileSize ] ;

	DWORD readingBytes;
	BOOL status = ReadFile(
									picFile,
									picFileBuffer,
									picFileSize,
									&readingBytes,
									NULL
								);

		CloseHandle( picFile ) ;

	if (( status == ERROR_HANDLE_EOF ) || ( readingBytes != picFileSize )) {
		ShowMessage( "Can't read file" );
		delete[] picFileBuffer;
		return 0;
	}

	// Check for valid
	BITMAPFILEHEADER BmpFileHeader;
	BITMAPINFOHEADER BmpInfoHeader;

	BmpFileHeader = *((BITMAPFILEHEADER*)&picFileBuffer[ 0 ]);
	BmpInfoHeader = *((BITMAPINFOHEADER*)&picFileBuffer[ sizeof( BITMAPFILEHEADER ) ]);

	if (	(BmpFileHeader.bfType != 0x4D42) ||
				(BmpFileHeader.bfReserved1 != 0) ||
				(BmpFileHeader.bfReserved2 != 0) ||
				(BmpFileHeader.bfSize != picFileSize) ) {
			ShowMessage( "This is not BMP file or incorrect BMP format." );
			delete[] picFileBuffer;
			return 0;
	}

	if (	(BmpInfoHeader.biSize != sizeof(BmpInfoHeader)) ||
				(BmpInfoHeader.biPlanes != 1) ||
				(BmpInfoHeader.biBitCount == 0) ||
				(BmpInfoHeader.biCompression != BI_RGB) ) {
		ShowMessage( "Incorrect BMP Format." );
		delete[] picFileBuffer;
		return 0;
	}

	if (BmpInfoHeader.biBitCount != 24) {
		ShowMessage( "This is not RGB (24 bit color) image." );
		delete[] picFileBuffer;
		return 0;
	}
	return picFileBuffer;
}
//---------------------------------------------------------------------------
void ShowPicture(const BYTE* picBuf) {
	BITMAPFILEHEADER BmpFileHeader;
	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);

	TMemoryStream* fStream;
	fStream = new TMemoryStream;

	fStream->Write(picBuf,BmpFileHeader.bfSize);
	fStream->Seek(0,0);

	Form2->Image1->Picture->Bitmap->LoadFromStream(fStream);

	delete fStream;
	Form2->Visible = true;

	return;
}
//---------------------------------------------------------------------------
BYTE* RGBtoGray (BYTE* picBuf) {
	BITMAPFILEHEADER BmpFileHeader;
	BITMAPINFOHEADER BmpInfoHeader;

	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);
	BmpInfoHeader = *((BITMAPINFOHEADER*)&picBuf[ sizeof( BITMAPFILEHEADER ) ]);

	long w = BmpInfoHeader.biWidth;
	long h = BmpInfoHeader.biHeight;
	long off = BmpFileHeader.bfOffBits;

	long addPix = 0;

	for (long i = 0; i < (w * h) ; i++) {
		BYTE tmp = .2989 * picBuf[ off + i * 3 +     addPix] +
				 		 + .5870 * picBuf[ off + i * 3 + 1 + addPix] +
				 		 + .1140 * picBuf[ off + i * 3 + 2 + addPix];
		picBuf[ off + i * 3 +     addPix] = tmp;
		picBuf[ off + i * 3 + 1 + addPix] = tmp;
		picBuf[ off + i * 3 + 2 + addPix] = tmp;
	}

	return picBuf;
}
//---------------------------------------------------------------------------
long  otsuThreshold(BYTE* picBuf) {

	BITMAPFILEHEADER BmpFileHeader;
	BITMAPINFOHEADER BmpInfoHeader;

	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);
	BmpInfoHeader = *((BITMAPINFOHEADER*)&picBuf[ sizeof( BITMAPFILEHEADER ) ]);

	long w = BmpInfoHeader.biWidth;
	long h = BmpInfoHeader.biHeight;
	long off = BmpFileHeader.bfOffBits;

	//Calculating additional pixels
	long oddPix = (4 - w * 3 % 4) * (w * 3 % 4 != 0);
	long calcPix = 0;
	long addPix = off;

	BYTE* pic;
	pic = new BYTE[ w * h ];

	for (long i = 0; i < (w * h) ; i++) {
		pic[i] = picBuf[ i * 3 + addPix];
		calcPix++;
		if (calcPix >= w) {
			calcPix -= w;
			addPix += oddPix;
		}
	}

	BYTE min = pic[0];
	BYTE max = pic[0];

	for (int i = 1; i < (w * h); i++) {
		if (pic[i] < min) min = pic[i];
		if (pic[i] > max) max = pic[i];
	}

	long histSize = max - min + 1;
	long *hist = new long[histSize];
	memset(hist, 0, histSize * sizeof(long));

	for (long i = 0; i < (w*h); i++) {
		hist[pic[i] - min]++;
	}

	long m = 0;
	long n = 0;

	for (long t = 0; t <= max - min; t++) {
		m += t * hist[t];
		n += hist[t];
	}

	float maxSigma = -1;
	long threshold = 0;
	long alpha1 = 0;
	long beta1 = 0;
	for (int t = 0; t < max - min; t++) {
		alpha1 += t * hist[t];
		beta1 += hist[t];
		float w1 = (float)beta1 / n;
		float a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);
		float sigma = w1 * (1 - w1) * a * a;

		if (sigma > maxSigma) {
		  maxSigma = sigma;
		  threshold = t;
		 }
	}
	threshold += min;

	delete[] pic;
	return threshold;
}
//---------------------------------------------------------------------------
void DrawHistogram(	BYTE* picBuf,
										BYTE amountHist,
										BYTE minColor = 0,
										BYTE maxColor = 255) {

	if ( (amountHist != 1) && (amountHist != 3) ) {
		ShowMessage("Wrong input data for function 'DrawHistogram'");
		return;
	}

	double* RHistogram;
	double* GHistogram;
	double* BHistogram;
	RHistogram = new double[ 256 ] ;
	GHistogram = new double[ 256 ] ;
	BHistogram = new double[ 256 ] ;

	if (amountHist == 1) {
		delete[] GHistogram;
		delete[] BHistogram;
		memset(RHistogram, 0, 255 * sizeof(double));
	 } else {
			memset(RHistogram, 0, 255 * sizeof(double));
			memset(GHistogram, 0, 255 * sizeof(double));
			memset(BHistogram, 0, 255 * sizeof(double));
	 }

	BITMAPFILEHEADER BmpFileHeader;
	BITMAPINFOHEADER BmpInfoHeader;

	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);
	BmpInfoHeader = *((BITMAPINFOHEADER*)&picBuf[ sizeof( BITMAPFILEHEADER ) ]);

	long w = BmpInfoHeader.biWidth;
	long h = BmpInfoHeader.biHeight;
	long off = BmpFileHeader.bfOffBits;

	long oddPix = (4 - w * 3 % 4) * (w * 3 % 4 != 0);
	long calcPix = 0;
	long addPix = off;

	if (amountHist == 1) {
		for (long i = 0; i < (w * h) ; i++) {
			if ((picBuf[ i * 3 + addPix] >= minColor) &&
					(picBuf[ i * 3 + addPix] <= maxColor)) {
					RHistogram[ picBuf[ i * 3 + addPix] ] ++ ;
			}
			calcPix++;
			if (calcPix >= w) {
				calcPix -= w;
				addPix += oddPix;
			}
		}
	} else {
		for (long i = 0; i < (w * h) ; i++) {
			if ((picBuf[ i * 3 + addPix] >= minColor) && (picBuf[ i * 3 + addPix] <= maxColor))
					RHistogram[ picBuf[ i * 3 +     addPix ] ] ++ ;

			if ((picBuf[ i * 3 + 1 + addPix] >= minColor) && (picBuf[ i * 3 + 1 + addPix] <= maxColor))
					GHistogram[ picBuf[ i * 3 + 1 + addPix] ] ++ ;

			if ((picBuf[ i * 3 + 2 + addPix] >= minColor) && (picBuf[ i * 3 + 2 + addPix] <= maxColor))
					BHistogram[ picBuf[ i * 3 + 2 + addPix] ] ++ ;

			calcPix++;
			if (calcPix >= w) {
					calcPix -= w;
					addPix += oddPix;
			}
		}
	}

	Form2->Visible = true;
	if(amountHist == 1) {
		Form2->Chart1->Series[0]->Active = true;
		Form2->Chart1->Series[1]->Active = false;
		Form2->Chart1->Series[2]->Active = false;
		Form2->Chart1->Series[0]->Clear();
		Form2->Chart1->Series[0]->AddArray(RHistogram, 255);
		Form2->Chart1->Visible = true;
	} else {
		Form2->Chart1->Series[0]->Active = true;
		Form2->Chart1->Series[1]->Active = true;
		Form2->Chart1->Series[2]->Active = true;
		Form2->Chart1->Series[0]->Clear();
		Form2->Chart1->Series[1]->Clear();
		Form2->Chart1->Series[2]->Clear();
		Form2->Chart1->Series[0]->AddArray(RHistogram, 255);
		Form2->Chart1->Series[1]->AddArray(GHistogram, 255);
		Form2->Chart1->Series[2]->AddArray(BHistogram, 255);
		Form2->Chart1->Visible = true;
		delete[] GHistogram;
		delete[] BHistogram;
	}

	delete[] RHistogram;
	return;
}
//---------------------------------------------------------------------------
BYTE* RGBRegionGrowing(	BYTE* picBuf,
												const float barrier) {

	BITMAPFILEHEADER BmpFileHeader;
	BITMAPINFOHEADER BmpInfoHeader;

	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);
	BmpInfoHeader = *((BITMAPINFOHEADER*)&picBuf[ sizeof( BITMAPFILEHEADER ) ]);

	long w = BmpInfoHeader.biWidth;
	long h = BmpInfoHeader.biHeight;
	long off = BmpFileHeader.bfOffBits;

	long oddPix = (4 - w * 3 % 4) * (w * 3 % 4 != 0);
	long calcPix = 0;
	long addPix = 0;

	long* map;
	map = new long[w * h];

	memset(map, 0, w * h * sizeof(long));

	long indexMap = 0;
	float* MxR, *MxG, *MxB;
	MxR = new float[w * h + 1];
	MxG = new float[w * h + 1];
	MxB = new float[w * h + 1];

	long* cSec;
	cSec = new long[w * h];

	BYTE* pic;
	pic = new BYTE[ w * h * 3];

	for (long i = 0; i < (w * h) ; i++) {
		pic[i*3]     = picBuf[ off + i * 3 +     addPix];
		pic[i*3 + 1] = picBuf[ off + i * 3 + 1 + addPix];
		pic[i*3 + 2] = picBuf[ off + i * 3 + 2 + addPix];

		calcPix++;

		if (calcPix >= w) {
			calcPix -= w;
			addPix += oddPix;
		}
	}

	MxB[0] = 0;
	MxG[0] = 0;
	MxR[0] = 0;

	for (long i = 0; i < (w * h) ; i++) {
		if (map[i] == 0) {
			indexMap++;
			map[i] = indexMap;
			MxB[indexMap] = pic[i * 3];
			MxG[indexMap] = pic[i * 3 + 1];
			MxR[indexMap] = pic[i * 3 + 2];

			cSec[indexMap] = 1;
			StaticSectorGrowing(pic, barrier, i, w, h, map, cSec, MxB, MxG, MxR, indexMap);
		}
	}



	for (long i = 0; i < (w * h) ; i++) {
		pic[i*3]     = BYTE(MxB[map[i]]);
		pic[i*3 + 1] = BYTE(MxG[map[i]]);
		pic[i*3 + 2] = BYTE(MxR[map[i]]);
	}

	delete[] map;
	delete[] MxR;
	delete[] MxG;
	delete[] MxB;
	delete[] cSec;

	calcPix = 0;
	addPix = 0;
	for (long i = 0; i < (w * h) ; i++) {
			picBuf[ off + i * 3 +     addPix] = pic[i*3] ;
			picBuf[ off + i * 3 + 1 + addPix] = pic[i*3 + 1];
			picBuf[ off + i * 3 + 2 + addPix] = pic[i*3 + 2];

			calcPix++;

			if (calcPix >= w) {
				calcPix -= w;
				addPix += oddPix;
			}
	}


	delete[] pic;
	return picBuf;
}
//---------------------------------------------------------------------------
void StaticSectorGrowing(BYTE* pic,
							 const float barrier,
							 const long start,
							 const long& w,
							 const long& h,
							 long* map,
							 long* cSec,
							 float* MxB,
							 float* MxG,
							 float* MxR,
							 long indexMap) {
	long x;
	long y;
	long i;

	long n_old = 1; // the number of current pixels for processing
	long n_new = 1; // the number of following pixels for processing
	long *arrN = new long [n_old];
	long *arrN_new;
	arrN[0] = start;

	while (n_new > 0) {
		n_old = n_new;
		n_new = 0;
		arrN_new = new long [n_old * 8];

		for (long j = 0; j < n_old; j++) {
			i = arrN[j];
			x = i % w;
			y = i / w;

			if (x > 0)
			 if (map[i - 1] == 0)
			  if (fabs(pic[(i - 1) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i - 1) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i - 1) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i - 1] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i - 1) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i - 1) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i - 1) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i - 1;
				 n_new++;
			}

			if (x < w - 1)
			 if (map[i + 1] == 0)
			  if (fabs(pic[(i + 1) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i + 1) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i + 1) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i + 1] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i + 1) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i + 1) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i + 1) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i + 1;
				 n_new++;
				}

			if (y > 0)
			 if (map[i - w] == 0)
			  if (fabs(pic[(i - w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i - w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i - w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i - w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i - w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i - w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i - w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i - w;
				 n_new++;
				}

			if (y < h - 1)
			 if (map[i + w] == 0)
			  if (fabs(pic[(i + w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i + w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i + w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i + w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i + w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i + w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i + w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i + w;
				 n_new++;
				}


			if (x > 0 && y > 0)
			 if (map[i - 1 - w] == 0)
			  if (fabs(pic[(i - 1 - w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i - 1 - w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i - 1 - w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i - 1 - w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i - 1 - w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i - 1 - w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i - 1 - w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i - 1 - w;
				 n_new++;
				}

			if (x > 0 && y < h - 1)
			 if (map[i - 1 + w] == 0)
			  if (fabs(pic[(i - 1 + w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i - 1 + w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i - 1 + w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i - 1 + w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i - 1 + w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i - 1 + w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i - 1 + w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i - 1 + w;
				 n_new++;
				}

			if (x < w - 1 && y < h - 1)
			 if (map[i + 1 + w] == 0)
			  if (fabs(pic[(i + 1 + w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i + 1 + w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i + 1 + w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i + 1 + w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i + 1 + w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i + 1 + w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i + 1 + w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i + 1 + w;
				 n_new++;
				}

			if (x < w - 1 && y > 0)
			 if (map[i + 1 - w] == 0)
			  if (fabs(pic[(i + 1 - w) * 3] - MxB[indexMap]) <= barrier)
			   if (fabs(pic[(i + 1 - w) * 3 + 1] - MxG[indexMap]) <= barrier)
				if (fabs(pic[(i + 1 - w) * 3 + 2] - MxR[indexMap]) <= barrier) {
				 map[i + 1 - w] = indexMap;
				 MxB[indexMap] = (MxB[indexMap]*cSec[indexMap] + pic[(i + 1 - w) * 3    ]) / (cSec[indexMap] + 1);
				 MxG[indexMap] = (MxG[indexMap]*cSec[indexMap] + pic[(i + 1 - w) * 3 + 1]) / (cSec[indexMap] + 1);
				 MxR[indexMap] = (MxR[indexMap]*cSec[indexMap] + pic[(i + 1 - w) * 3 + 2]) / (cSec[indexMap] + 1);
				 cSec[indexMap]++;
				 arrN_new[n_new] = i + 1 - w;
				 n_new++;
			}
		}

		delete[] arrN;
		arrN = arrN_new;
	}

	delete[] arrN_new;
	return;
}

//---------------------------------------------------------------------------
void WritePicture(const BYTE* picBuf, const char* name) {

	BITMAPFILEHEADER BmpFileHeader;
	BmpFileHeader = *((BITMAPFILEHEADER*)&picBuf[ 0 ]);

	TMemoryStream* fStream;

	fStream = new TMemoryStream;

	fStream->Write(picBuf,BmpFileHeader.bfSize);
	fStream->Seek(0,0);
	fStream->SaveToFile(name);
	delete fStream;
}
//---------------------------------------------------------------------------
