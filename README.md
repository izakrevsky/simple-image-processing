Simple Image Processing
=======================
This repository contains my first expirience with image processing.

This code works with simple bmp files. Can open, write and show them without any additional libraries.

Main abilities:

* RGB to grayscale convertation
* RGB Region Growing
* Otsu Thresholding
* Draw Colors Distribution Histogram

Why am I not using OpenCV for it? I have no idea, it was interesting for me.

Program was written for Borland Studio.

It should be rewritten, but I have no idea, when I will do it.
